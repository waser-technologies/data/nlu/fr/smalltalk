# Papotage: Domaine NLU

Ce domaine est conçu pour standardiser les classes pour les tâches de classification d'intentions.
Ce domaine n'est pas spécifique. Il s'agit du domaine contenant les régles d'usage d'une conversation, la politesse, salutations. etc 

## Installation

Utilisez l'[Outil de Gestion de Domaines](https://gitlab.com/waser-technologies/technologies/dmt) (DMT en anglais) pour installer, évaluer et entrainer un modèle sur ce domaine.

```zsh
dmt -V -T -a https://gitlab.com/waser-technologies/data/nlu/fr/smalltalk.git
```

## Utilisation

Utiliser la commande `dmt` pour lancer un serveur avec le dernier modèle entraîné.

```zsh
dmt -S
```

Cela va prendre du temps.

Attendez de voir:
> `INFO     root  - Rasa server is up and running.`

Vous pourrez alors tester le modèle.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "Salut", "sender": "utilisateur" }'
```

Ou utilisez simplement [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant Salut
```

## Structure des données

Vous trouverez l'ensemble des données dans le dossier `data/`.


### Classification des intentions et reconnaissance des entités

Sous `data/nlu/`, vous trouverez tout ce qui concerne l'extraction des classes d'intention et des entités.

```zsh
.
├── data/
│ ├── nlu/
│ │ ├── *.yml
│ │ └── verbs.yml
```

> `verbs.yml`
> est le fichier principal.
> Il contient des *intentions* pour les *verbes*.

> `*.yml`
> -> Lorsqu'un *verbe* est suffisamment complexe, il est déplacé dans son propre fichier.

> `/!\` Pour chaque *verbe* avec son propre fichier, vous devez créer un [ResponseSelector]() pour le *verbe*.

#### *Intention*:

Un [*verbe*], sa [*cible*], son [*sujet*], le [*sujet de sa cible*].

> Mais ça se prononce [*sujet de la cible*] [*cible*] [*sujet*] [*verbe*].

```yml
#!data/nlu/pouvoir.yml

version : "3.1"
nlu :
# parler tu ne peux pas
- intent : pouvoir_pas/parler_tu
  examples : |
    - Tu ne peux pas parler
    - Vous n'avez pas le droit de parler
    - Ne parle pas
    - silencieux
    - silence
```

```yml
#!data/nlu/imaginer.yml

version : "3.1"
nlu :
# intention : <VERB>(/<VERB_TARGET>)(_<VERB_SUBJECT>)(_<VERB_TARGET_SUBJECT>)
- intent: imaginer/apparence_tu_je
# Pour mieux comprendre une intention, essayez de la dire comme ceci :
# <VERB_TARGET_SUBJECT> <VERB_TARGET> <VERB_SUBJECT> <VERB>
# moi apparence tu imagines
# Ou si vous préférez : Mon apparence, tu imagines.
  examples : |
    - à quoi pensez-vous que je ressemble
    - imagine à quoi je ressemble
```

#### *Entité* :

Un bon outil de remplissage de *slots*.

Les entités sont entourées de crochets et suivies de leur type entre parenthèses.

```yml
#!data/nlu/speak.yml

version : "3.1"
nlu :
# langues que tu parler (rappel, cible' sujet, cible, verbe' sujet et verbe)
- intent : parler/quel_tu_langue
  examples : |
    - Parlez-vous [allemand](langue) ?
    - Comprenez-vous l'[espagnol](langue) ?
```

#### *Réponse*:

Énoncé qui répond à une *intention*.

```zsh
.
├── data/
│ ├── nlu/
│ │ ├── responses/
│ │ │ ├── *.yml
│ │ │ └──responses.yml
```

> `responses.yml`
> est le fichier principal.
> Il contient des *réponses* aux *intentions*.

> `*.yml`
> -> Il y a un fichier *response* pour chaque *verbe* qui a son propre fichier.

> `(i)`
> Le fichier *response* est nommé d'après le *verbe* auquel il répond.
> (ex : `data/nlu/être.yml` -> `data/nlu/responses/être.yml`)

Une *réponse* est une action de base qui renvoie un texte statique basé sur l'intention.

Elle commence toujours par `utter_` suivi du nom de l'*intention* à laquelle elle répond.

Par conséquent, 'utter' devient 'répond'.

Ce qui donne : `répond {intent}`.

Voici un exemple concret extrait de `excuser`.

```yml
#!data/nlu/excuser.yml


version : "3.1"
nlu :
# Intention : tu excuser
- intent : excuser/je_tu
  examples : |
    - excuse-toi
    - observez-vous
    - s'excuser
    - s'excuser maintenant
    - te racheter
    - mes excuses
# Intention : m'excuser
- intent : excuser/tu_je
  examples : |
    - pardon
    - Je m'excuse
    - Pardon
    - Je suis désolé
```


```yml
#!data/nlu/responses/excuser.yml

version : "3.1"
réponses :
# Action : répondez en vous excusant
# Ici, l'intention ne contient qu'un *verbe*.
# Il doit être compris comme une injonction.
# par exemple : 'Excusez-vous, maintenant !'
  utter_excuser :
  - text : Mes excuses.
  - text : je suis profondément désolé.
  - text : je suis vraiment désolé.
  - text : Mes excuses
  - text : je voudrais que vous me pardonniez
  - text : Pardonnez-moi.
  - text : vous devez savoir que je suis désolé.

# Action : répondez-moi, excusez-moi
# Ici *moi* veut *s'excuser* de *vous*.
# Ce qui revient essentiellement à dire *s'excuser*.
# Nous ne nous répéterons pas,
# nous utiliserons à la place la réponse par défaut de ce verbe.
# par exemple : 'Excusez-vous !'
  utter_excuser/je_tu :
  - template : utter_excuser

# Action : répondez-moi excusez-moi
# Ici, l'intention décrit *moi* *s'excuser* auprès de *vous*.
# Cela doit être compris comme une excuse de *moi*.
# ex. : 'Désolé'
  utter_excuser/tu_je :
  - text : je te pardonne.
  - text : Vous êtes excusé.
  - text : Eh bien, s'il vous plaît soyez attentif.
  - text : oui.
  - text : N'en parlons plus.
  - text : je n'ai pas besoin de l'entendre.
  - text : Pas besoin, vraiment.
  - text : vous n'avez pas besoin de le dire.
  - text : oubliez-le !
```

### *Histoires*:

Lisez-moi une *histoire*

```zsh
.
├── data/
│ ├── stories/
│ │ ├── *.yml
│ │ └── stories.yml
```

```yml
#!data/stories/stories.yml

version : "3.1"
stories:
- story: l'utilisateur est heureux de rencontrer l'assistant
  steps:
  - intent : saluer_tu_je_arriver # Salut
  - action : utter_saluer_tu_je_arriver # Salut
  - intent : être # Ravi de vous rencontrer
  - action : utter_être # C'est vraiment mon plaisir
```
